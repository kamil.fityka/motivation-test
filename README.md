Test symfony project build on Symfony 6.1.

## Getting Started

1. Run `docker-compose build --pull --no-cache` to build fresh images
2. Run `docker-compose up -d` to run the Docker containers
3. Open `http://localhost:8050` in your favorite web browser
4. Run `docker-compose down --remove-orphans` to stop the Docker containers.
5. Run `docker exec -it motivation-test-php-1 bash` to get into PHP container
   1. Run `composer install` if composer not installed
   2. Run `php bin/console doctrine:fixtures:load` if you need basic data

## Tests

1. Run `docker exec -it motivation-test-php-1 bash` to get into PHP container
2. Run `php bin/phpunit` to run tests


## Others:

1. If need to clean up code run `php vendor/bin/php-cs-fixer fix xxxx` where `xxxx` is catalog name e.x. `php vendor/bin/php-cs-fixer fix src` will clean up src catalog.
2. If no database set go into PHP container and run `php bin/console d:m:m` and `php bin/console d:m:m --env=test` for test environment