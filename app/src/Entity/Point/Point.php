<?php

declare(strict_types=1);

namespace App\Entity\Point;

use App\Entity\Action\Activity;
use App\Entity\Action\Booster;
use App\Entity\User\User;
use App\Point\Repository\PointRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

#[ORM\Entity(repositoryClass: PointRepository::class)]
class Point
{
    use TimestampableEntity;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\Column(type: 'integer')]
    private int $value;

    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(name: 'user_id', referencedColumnName: 'id')]
    private User $user;

    #[ORM\ManyToOne(targetEntity: Activity::class)]
    #[ORM\JoinColumn(name: 'action_id', referencedColumnName: 'id')]
    private ?Activity $activity = null;

    #[ORM\ManyToOne(targetEntity: Booster::class)]
    #[ORM\JoinColumn(name: 'booster_id', referencedColumnName: 'id')]
    private ?Booster $booster = null;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?\DateTime $validTo = null;

    #[ORM\Column(type: 'boolean', options: ['default' => false])]
    private bool $paidOut = false;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getValue(): ?int
    {
        return $this->value;
    }

    public function setValue(int $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getActivity(): ?Activity
    {
        return $this->activity;
    }

    public function setActivity(?Activity $activity): self
    {
        $this->activity = $activity;

        return $this;
    }

    public function getBooster(): ?Booster
    {
        return $this->booster;
    }

    public function setBooster(?Booster $booster): self
    {
        $this->booster = $booster;

        return $this;
    }

    public function getValidTo(): ?\DateTime
    {
        return $this->validTo;
    }

    public function setValidTo(?\DateTime $validTo): self
    {
        $this->validTo = $validTo;

        return $this;
    }

    public function isPaidOut(): ?bool
    {
        return $this->paidOut;
    }

    public function setPaidOut(bool $paidOut): self
    {
        $this->paidOut = $paidOut;

        return $this;
    }
}
