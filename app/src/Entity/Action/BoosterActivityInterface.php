<?php

declare(strict_types=1);

namespace App\Entity\Action;

interface BoosterActivityInterface
{
    public function getBooster(): ?Booster;

    public function setBooster(?Booster $booster): self;
}
