<?php

declare(strict_types=1);

namespace App\Entity\Action;

use App\Action\Repository\DeliveryRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: DeliveryRepository::class)]
class Delivery extends Activity
{
}
