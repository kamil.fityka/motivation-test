<?php

declare(strict_types=1);

namespace App\Entity\Action;

use App\Action\Repository\BoosterRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: BoosterRepository::class)]
class Booster
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\Column(type: 'string')]
    private string $name;

    #[ORM\Column(type: 'integer')]
    private int $pointsValue;

    #[ORM\Column(type: 'integer')]
    private int $actionCondition;

    #[ORM\Column(type: 'integer')]
    private int $hourCondition;

    #[ORM\OneToOne(inversedBy: 'booster', targetEntity: Activity::class)]
    #[ORM\JoinColumn(name: 'activity_id', referencedColumnName: 'id')]
    private ?Activity $activity = null;

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPointsValue(): int
    {
        return $this->pointsValue;
    }

    public function setPointsValue(int $pointsValue): self
    {
        $this->pointsValue = $pointsValue;

        return $this;
    }

    public function getActionCondition(): int
    {
        return $this->actionCondition;
    }

    public function setActionCondition(int $actionCondition): self
    {
        $this->actionCondition = $actionCondition;

        return $this;
    }

    public function getHourCondition(): int
    {
        return $this->hourCondition;
    }

    public function setHourCondition(int $hourCondition): self
    {
        $this->hourCondition = $hourCondition;

        return $this;
    }

    public function getActivity(): ?Activity
    {
        return $this->activity;
    }

    public function setActivity(?Activity $activity): void
    {
        $this->activity = $activity;
    }
}
