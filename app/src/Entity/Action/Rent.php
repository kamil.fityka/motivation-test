<?php

declare(strict_types=1);

namespace App\Entity\Action;

use App\Action\Repository\RentRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RentRepository::class)]
class Rent extends Activity
{
}
