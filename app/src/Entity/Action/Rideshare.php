<?php

declare(strict_types=1);

namespace App\Entity\Action;

use App\Action\Repository\RideshareRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RideshareRepository::class)]
class Rideshare extends Activity
{
}
