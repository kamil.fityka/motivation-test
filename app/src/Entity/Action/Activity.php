<?php

declare(strict_types=1);

namespace App\Entity\Action;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity()]
#[ORM\Table(name: 'activities')]
#[ORM\InheritanceType('JOINED')]
#[ORM\DiscriminatorColumn(name: 'type', type: 'string')]
#[ORM\DiscriminatorMap([
    'delivery' => Delivery::class,
    'rideshare' => Rideshare::class,
    'rent' => Rent::class,
])]
class Activity implements BoosterActivityInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\Column(type: 'string', unique: true)]
    private string $name;

    #[ORM\Column(type: 'integer')]
    private int $pointsValue;

    #[ORM\OneToOne(mappedBy: 'activity', targetEntity: Booster::class, cascade: ['persist'])]
    private ?Booster $booster = null;

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPointsValue(): int
    {
        return $this->pointsValue;
    }

    public function setPointsValue(int $pointsValue): self
    {
        $this->pointsValue = $pointsValue;

        return $this;
    }

    public function getBooster(): ?Booster
    {
        return $this->booster;
    }

    public function setBooster(?Booster $booster): self
    {
        $this->booster = $booster;

        return $this;
    }
}
