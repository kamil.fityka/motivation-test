<?php

declare(strict_types=1);

namespace App\Point\Service;

use App\Action\Model\ActivityModel;
use App\Entity\Action\Booster;
use App\Entity\Point\Point;
use App\Entity\User\User;
use Doctrine\ORM\EntityManagerInterface;

class CreatePointService
{
    public function __construct(
        private EntityManagerInterface $entityManager
    ) {
    }

    public function createActivityPoints(ActivityModel $model): void
    {
        $points = (new Point())
            ->setActivity($model->getActivity())
            ->setUser($model->getUser())
            ->setValue($model->getPoints())
            ->setCreatedAt(new \DateTime())
            ->setUpdatedAt(new \DateTime());

        $this->entityManager->persist($points);
        $this->entityManager->flush();
    }

    public function createBoosterPoints(Booster $booster, User $user): void
    {
        $points = (new Point())
            ->setBooster($booster)
            ->setUser($user)
            ->setValue($booster->getPointsValue())
            ->setValidTo(new \DateTime('+1 month'))
            ->setCreatedAt(new \DateTime())
            ->setUpdatedAt(new \DateTime());

        $this->entityManager->persist($points);
        $this->entityManager->flush();
    }
}
