<?php

declare(strict_types=1);

namespace App\Point\Repository;

use App\Entity\Action\Booster;
use App\Entity\Point\Point;
use App\Entity\User\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Point>
 *
 * @method Point|null find($id, $lockMode = null, $lockVersion = null)
 * @method Point|null findOneBy(array $criteria, array $orderBy = null)
 * @method Point[]    findAll()
 * @method Point[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PointRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Point::class);
    }

    public function add(Point $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Point $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function countUserActivityBooster(Booster $booster): int
    {
        $date = new \DateTime();
        $date->modify(sprintf('-%d hour', $booster->getHourCondition()));

        return (int) $this->createQueryBuilder('p')
            ->select('COUNT(p.id)')
            ->where('p.activity = :activity')
            ->andWhere('p.createdAt > :date')
            ->setParameters([
                'activity' => $booster->getActivity(),
                'date' => $date,
            ])
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function findLastActionsByBoosterActionConditions(Booster $booster): array
    {
        $date = new \DateTime();
        $date->modify(sprintf('-%d hour', $booster->getHourCondition()));

        return $this->createQueryBuilder('p')
            ->andWhere('p.createdAt > :date')
            ->setParameters([
                'date' => $date,
            ])
            ->setMaxResults($booster->getActionCondition())
            ->orderBy('p.id', 'DESC')
            ->getQuery()
            ->getResult();
    }

    public function sumUserBalance(User $user): int
    {
        $sql = 'SELECT SUM(p.points) as points FROM(
            SELECT SUM(`value`) as points FROM point WHERE action_id IS NOT NULL AND paid_out = 0 AND user_id = :userId
            UNION ALL
            SELECT SUM(`value`) as points FROM point WHERE booster_id IS NOT NULL AND paid_out = 0 AND valid_to >= :date AND user_id = :userId
        ) AS p';

        $conn = $this->getEntityManager()->getConnection();
        $query = $conn->prepare($sql);
        $result = $query->executeQuery([
                'userId' => $user->getId(),
                'date' => (new \DateTime())->modify('-1 month')->format('Y-m-d H:i:s'),
            ])
            ->fetchOne();

        return (int) $result;
    }
}
