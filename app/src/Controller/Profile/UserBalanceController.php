<?php

declare(strict_types=1);

namespace App\Controller\Profile;

use App\User\Service\UserBalanceService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserBalanceController extends AbstractController
{
    #[Route(path: '/profile/balance', name: 'homepage')]
    public function balance(UserBalanceService $service): Response
    {
        return $this->render('profile/balance.html.twig', [
            'balance' => $service->getBalance(),
        ]);
    }
}
