<?php

declare(strict_types=1);

namespace App\Controller\Action;

use App\Action\Service\UserActivityService;
use App\Entity\Action\Activity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ActivityController extends AbstractController
{
    public function __construct(
        private UserActivityService $activityService
    ) {
    }

    #[Route(path: '/activity/{id}/{duration}', name: 'make_activity')]
    public function process(Activity $activity, int $duration = 1): Response
    {
        $this->activityService->createAction($activity, $this->getUser(), $duration);

        return $this->render('homepage/index.html.twig');
    }
}
