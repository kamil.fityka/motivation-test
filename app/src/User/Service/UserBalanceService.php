<?php

declare(strict_types=1);

namespace App\User\Service;

use App\Point\Repository\PointRepository;
use Symfony\Component\Security\Core\Security;

class UserBalanceService
{
    public function __construct(
        private PointRepository $repository,
        private Security $security
    ) {
    }

    public function getBalance(): int
    {
        return $this->repository->sumUserBalance($this->security->getUser());
    }
}
