<?php

declare(strict_types=1);

namespace App\Action\DataFixture;

use App\Entity\Action\Booster;
use App\Entity\Action\Delivery;
use App\Entity\Action\Rent;
use App\Entity\Action\Rideshare;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ActionFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $deliverBooster = (new Booster())
            ->setName('Dlivery booster')
            ->setPointsValue(5)
            ->setActionCondition(5)
            ->setHourCondition(2);
        $deliver = (new Delivery())
            ->setName('Deliver')
            ->setPointsValue(1)
            ->setBooster($deliverBooster);
        $deliverBooster->setActivity($deliver);

        $manager->persist($deliverBooster);
        $manager->persist($deliver);
        $manager->flush();

        $rideshareBooster = (new Booster())
            ->setName('Rideshare booster')
            ->setPointsValue(10)
            ->setActionCondition(5)
            ->setHourCondition(8);
        $rideshare = (new Rideshare())
            ->setName('Rideshare')
            ->setPointsValue(1)
            ->setBooster($rideshareBooster);
        $rideshareBooster->setActivity($rideshare);

        $manager->persist($rideshareBooster);
        $manager->persist($rideshare);
        $manager->flush();

        $rent = (new Rent())
            ->setName('Rent')
            ->setPointsValue(2);

        $manager->persist($rent);
        $manager->flush();
    }
}
