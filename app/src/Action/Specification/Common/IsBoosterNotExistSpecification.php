<?php

declare(strict_types=1);

namespace App\Action\Specification\Common;

use App\Entity\Action\Booster;

class IsBoosterNotExistSpecification
{
    public function isSatisfiedBy(?Booster $booster = null): bool
    {
        if (!$booster) {
            return false;
        }

        return true;
    }
}
