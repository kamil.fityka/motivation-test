<?php

declare(strict_types=1);

namespace App\Action\Specification\Common;

use App\Action\Specification\Specification;
use App\Entity\Action\Booster;
use App\Entity\Point\Point;
use App\Point\Repository\PointRepository;

class NoBoosterInPreviousActionConditionSpecification implements Specification
{
    public function __construct(private PointRepository $repository)
    {
    }

    public function isSatisfiedBy(Booster $booster): bool
    {
        $points = $this->repository->findLastActionsByBoosterActionConditions($booster);

        /** @var Point $point */
        foreach ($points as $point) {
            if ($point->getBooster()) {
                return false;
            }
        }

        return true;
    }
}
