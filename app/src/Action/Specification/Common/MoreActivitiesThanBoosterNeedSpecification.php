<?php

declare(strict_types=1);

namespace App\Action\Specification\Common;

use App\Action\Specification\Specification;
use App\Entity\Action\Booster;
use App\Point\Repository\PointRepository;

class MoreActivitiesThanBoosterNeedSpecification implements Specification
{
    public function __construct(private PointRepository $repository)
    {
    }

    public function isSatisfiedBy(Booster $booster): bool
    {
        $activities = $this->repository->countUserActivityBooster($booster);

        return $activities >= $booster->getActionCondition();
    }
}
