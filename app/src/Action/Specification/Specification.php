<?php

declare(strict_types=1);

namespace App\Action\Specification;

use App\Entity\Action\Booster;

interface Specification
{
    public function isSatisfiedBy(Booster $booster): bool;
}
