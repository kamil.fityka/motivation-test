<?php

declare(strict_types=1);

namespace App\Action\Specification;

use App\Action\Specification\Common\IsBoosterNotExistSpecification;
use App\Action\Specification\Common\MoreActivitiesThanBoosterNeedSpecification;
use App\Action\Specification\Common\NoBoosterInPreviousActionConditionSpecification;
use App\Entity\Action\Booster;
use App\Point\Repository\PointRepository;

class DeliveryBoosterSpecification implements BoosterSpecification
{
    public function __construct(private PointRepository $repository)
    {
    }

    public function isSatisfiedBy(?Booster $booster = null): bool
    {
        $boosterExistSpecification = new IsBoosterNotExistSpecification();
        if (!$boosterExistSpecification->isSatisfiedBy($booster)) {
            return false;
        }

        $specification = (new AndSpecification())
            ->add(new MoreActivitiesThanBoosterNeedSpecification($this->repository))
            ->add(new NoBoosterInPreviousActionConditionSpecification($this->repository));

        return $specification->isSatisfiedBy($booster);
    }
}
