<?php

declare(strict_types=1);

namespace App\Action\Specification;

use App\Entity\Action\Booster;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class AndSpecification implements Specification
{
    private Collection $specifications;

    public function __construct()
    {
        $this->specifications = new ArrayCollection();
    }

    public function add(Specification $specification): self
    {
        if (!$this->specifications->contains($specification)) {
            $this->specifications[] = $specification;
        }

        return $this;
    }

    public function isSatisfiedBy(Booster $booster): bool
    {
        foreach ($this->specifications as $specification) {
            if (!$specification->isSatisfiedBy($booster)) {
                return false;
            }
        }

        return true;
    }
}
