<?php

declare(strict_types=1);

namespace App\Action\Model;

use App\Entity\Action\Activity;
use App\Entity\Action\Booster;
use App\Entity\Action\BoosterActivityInterface;
use App\Entity\Action\Rent;
use App\Entity\User\User;
use JetBrains\PhpStorm\Pure;

class ActivityModel
{
    private User $user;
    private Activity $activity;
    private ?Booster $booster = null;
    private int $points;

    public function __construct(User $user, Activity $activity, int $duration = 1)
    {
        $this->user = $user;
        $this->activity = $activity;
        $this->points = $this->countPoints($activity, $duration);

        if ($activity instanceof BoosterActivityInterface) {
            $this->booster = $activity->getBooster();
        }
    }

    #[Pure]
    private function countPoints(Activity $activity, int $duration): int
    {
        if ($activity instanceof Rent) {
            return $activity->getPointsValue() * $duration;
        }

        return $activity->getPointsValue();
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getActivity(): Activity
    {
        return $this->activity;
    }

    public function hasBooster(): bool
    {
        return $this->booster instanceof Booster;
    }

    public function getBooster(): ?Booster
    {
        return $this->booster;
    }

    public function getPoints(): int
    {
        return $this->points;
    }
}
