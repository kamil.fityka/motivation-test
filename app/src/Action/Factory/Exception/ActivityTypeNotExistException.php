<?php

declare(strict_types=1);

namespace App\Action\Factory\Exception;

class ActivityTypeNotExistException extends \Exception
{
    public function __construct(string $className, int $code = 0, ?\Throwable $previous = null)
    {
        $message = sprintf('Action type not found: %s', $className);

        parent::__construct($message, $code, $previous);
    }
}
