<?php

declare(strict_types=1);

namespace App\Action\Factory;

use App\Action\Factory\Exception\ActivityTypeNotExistException;
use App\Action\Service\UserActivityInterface;

class UserActivityFactory
{
    public function __construct(
        private iterable $actions,
    ) {
    }

    public function create(string $className): UserActivityInterface
    {
        /** @var UserActivityInterface $action */
        foreach ($this->actions as $action) {
            if ($action->isSupported($className)) {
                return $action;
            }
        }

        throw new ActivityTypeNotExistException($className);
    }
}
