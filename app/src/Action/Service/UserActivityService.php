<?php

declare(strict_types=1);

namespace App\Action\Service;

use App\Action\Factory\Exception\ActivityTypeNotExistException;
use App\Action\Factory\UserActivityFactory;
use App\Action\Model\ActivityModel;
use App\Entity\Action\Activity;
use App\Entity\User\User;
use Psr\Log\LoggerInterface;

class UserActivityService
{
    public function __construct(
        private UserActivityFactory $factory,
        private LoggerInterface $logger,
    ) {
    }

    public function createAction(Activity $activity, User $user, int $duration = 1): void
    {
        try {
            $actionService = $this->factory->create($activity::class);
        } catch (ActivityTypeNotExistException $e) {
            $this->logger->error($e->getMessage());
        }

        $activityModel = new ActivityModel($user, $activity, $duration);

        $actionService->handle($activityModel);
    }
}
