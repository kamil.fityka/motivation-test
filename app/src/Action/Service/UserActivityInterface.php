<?php

declare(strict_types=1);

namespace App\Action\Service;

use App\Action\Model\ActivityModel;

interface UserActivityInterface
{
    public function isSupported(string $className): bool;

    public function handle(ActivityModel $model): void;
}
