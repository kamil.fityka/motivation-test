<?php

declare(strict_types=1);

namespace App\Action\Service\Common;

use App\Action\Model\ActivityModel;
use App\Action\Service\UserActivityInterface;
use App\Entity\Action\Rent;
use App\Point\Service\CreatePointService;

class RentActivityService implements UserActivityInterface
{
    public function __construct(
        private CreatePointService $pointService
    ) {
    }

    public function isSupported(string $className): bool
    {
        return Rent::class === $className;
    }

    public function handle(ActivityModel $model): void
    {
        $this->pointService->createActivityPoints($model);
    }
}
