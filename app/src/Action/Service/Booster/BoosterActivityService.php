<?php

declare(strict_types=1);

namespace App\Action\Service\Booster;

use App\Action\Model\ActivityModel;
use App\Action\Specification\BoosterSpecification;
use App\Point\Service\CreatePointService;

abstract class BoosterActivityService
{
    public function __construct(
        private BoosterSpecification $specification,
        private CreatePointService $pointService
    ) {
    }

    public function handle(ActivityModel $model): void
    {
        $this->pointService->createActivityPoints($model);

        if ($model->hasBooster()) {
            $booster = $model->getBooster();
            if ($this->specification->isSatisfiedBy($booster)) {
                $this->pointService->createBoosterPoints($booster, $model->getUser());
            }
        }
    }
}
