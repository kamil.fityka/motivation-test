<?php

declare(strict_types=1);

namespace App\Action\Service\Booster;

use App\Action\Service\UserActivityInterface;
use App\Action\Specification\DeliveryBoosterSpecification;
use App\Entity\Action\Rideshare;
use App\Point\Service\CreatePointService;
use JetBrains\PhpStorm\Pure;

class RideshareActivityService extends BoosterActivityService implements UserActivityInterface
{
    #[Pure]
    public function __construct(
        protected DeliveryBoosterSpecification $specification,
        protected CreatePointService $pointService
    ) {
        parent::__construct(
            $this->specification,
            $this->pointService
        );
    }

    public function isSupported(string $className): bool
    {
        return Rideshare::class === $className;
    }
}
