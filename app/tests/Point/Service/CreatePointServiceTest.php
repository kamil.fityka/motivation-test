<?php

declare(strict_types=1);

namespace App\Tests\User\Service;

use App\Action\Model\ActivityModel;
use App\Entity\Action\Booster;
use App\Entity\Action\BoosterActivityInterface;
use App\Entity\Action\Delivery;
use App\Entity\Action\Rent;
use App\Entity\Action\Rideshare;
use App\Entity\Point\Point;
use App\Entity\User\User;
use App\Point\Service\CreatePointService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class CreatePointServiceTest extends KernelTestCase
{
    private ?EntityManagerInterface $entityManager;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @dataProvider getSpecificDuration
     */
    public function testCreateNewPointsForOneDayRent(int $duration): void
    {
        $rent = (new Rent())
            ->setName('Rent')
            ->setPointsValue(2);

        $this->entityManager->persist($rent);

        $user = (new User())
            ->setName('Joe')
            ->setEmail('joe@email.com')
            ->setPassword('password');

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $model = new ActivityModel($user, $rent, $duration);
        $pointService = new CreatePointService($this->entityManager);
        $pointService->createActivityPoints($model);
        $pointRepository = $this->entityManager->getRepository(Point::class);
        $points = $pointRepository->sumUserBalance($user);

        $this->assertEquals($points, $rent->getPointsValue() * $duration);
    }

    /**
     * @dataProvider getBoosterActivityInterfaces
     */
    public function testCreateNewPointsForOneBoosterActivityInterfaceWithoutBooster(BoosterActivityInterface $activity): void
    {
        $this->entityManager->persist($activity);

        $user = (new User())
            ->setName('Joe')
            ->setEmail('joe@email.com')
            ->setPassword('password');

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $model = new ActivityModel($user, $activity);
        $pointService = new CreatePointService($this->entityManager);
        $pointService->createActivityPoints($model);
        $pointRepository = $this->entityManager->getRepository(Point::class);
        $points = $pointRepository->sumUserBalance($user);

        $this->assertEquals($points, $activity->getPointsValue());
    }

    /**
     * @dataProvider getBoosterActivity
     */
    public function testCreateNewPointsForOneBoosterActivityInterfaceWithBooster(BoosterActivityInterface $activity): void
    {
        $this->entityManager->persist($activity);

        $user = (new User())
            ->setName('Joe')
            ->setEmail('joe@email.com')
            ->setPassword('password');

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $model = new ActivityModel($user, $activity);
        $booster = $model->getBooster();
        $pointService = new CreatePointService($this->entityManager);

        $iterations = $booster->getActionCondition();
        for ($i = 0; $i < $iterations; ++$i) {
            $pointService->createActivityPoints($model);
        }

        $pointService->createBoosterPoints($booster, $model->getUser());

        $pointRepository = $this->entityManager->getRepository(Point::class);
        $points = $pointRepository->sumUserBalance($user);

        $sum = ($activity->getPointsValue() * $iterations) + $booster->getPointsValue();

        $this->assertEquals($points, $sum);
    }

    public function getBoosterActivity(): array
    {
        return [
            [
                (new Delivery())
                    ->setName('Delivery')
                    ->setPointsValue(1)
                    ->setBooster(
                        (new Booster())
                            ->setName('Dlivery booster')
                            ->setPointsValue(5)
                            ->setActionCondition(5)
                            ->setHourCondition(2)
                    ),
            ],
            [
                (new Rideshare())
                    ->setName('Rideshare')
                    ->setPointsValue(1)
                    ->setBooster(
                        (new Booster())
                            ->setName('Rideshare booster')
                            ->setPointsValue(10)
                            ->setActionCondition(5)
                            ->setHourCondition(8)
                    ),
            ],
        ];
    }

    public function getBoosterActivityInterfaces(): array
    {
        return [
            [
                (new Delivery())
                    ->setName('Delivery')
                    ->setPointsValue(1),
            ],
            [
                (new Rideshare())
                    ->setName('Rideshare')
                    ->setPointsValue(1),
            ],
        ];
    }

    public function getSpecificDuration(): array
    {
        return [
            [1],
            [3],
            [9],
        ];
    }

    protected function tearDown(): void
    {
        $this->entityManager->getConnection()->rollback();

        parent::tearDown();

        $this->entityManager->close();
        $this->entityManager = null;
    }
}
