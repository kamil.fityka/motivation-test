<?php

declare(strict_types=1);

namespace App\Tests\Action\Service;

use App\Action\Service\UserActivityService;
use App\Entity\Action\Activity;
use App\Entity\Action\Booster;
use App\Entity\Action\BoosterActivityInterface;
use App\Entity\Action\Delivery;
use App\Entity\Action\Rent;
use App\Entity\Action\Rideshare;
use App\Entity\Point\Point;
use App\Entity\User\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class UserActivityServiceTest extends KernelTestCase
{
    private ?EntityManagerInterface $entityManager;
    private UserActivityService $userActivityService;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();
        $container = $kernel->getContainer();

        $this->entityManager = $container
            ->get('doctrine')
            ->getManager();

        $this->userActivityService = $container->get(UserActivityService::class);

        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @dataProvider getActivity
     */
    public function testUserBalanceForOnlyRentNoBooster(array $activities): void
    {
        foreach ($activities as $activity) {
            $this->entityManager->persist($activity);
        }

        $user = (new User())
            ->setName('Joe')
            ->setEmail('joe@email.com')
            ->setPassword('password');

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $sum = 0;
        /** @var Activity $activity */
        foreach ($activities as $activity) {
            $this->userActivityService->createAction($activity, $user);
            $sum += $activity->getPointsValue();
        }

        $pointRepository = $this->entityManager->getRepository(Point::class);
        $points = $pointRepository->sumUserBalance($user);

        $this->assertEquals($points, $sum);
    }

    /**
     * @dataProvider getActivity
     */
    public function testUserBalanceForOnlyRentWithBooster(array $activities): void
    {
        foreach ($activities as $activity) {
            $this->entityManager->persist($activity);
        }

        $user = (new User())
            ->setName('Joe')
            ->setEmail('joe@email.com')
            ->setPassword('password');

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $sum = 0;
        /** @var Activity $activity */
        foreach ($activities as $activity) {
            if ($activity instanceof BoosterActivityInterface) {
                $booster = $activity->getBooster();
                $iterations = $booster->getActionCondition();
                for ($i = 0; $i < $iterations; ++$i) {
                    $this->userActivityService->createAction($activity, $user);
                    $sum += $activity->getPointsValue();
                }
                $sum += $booster->getPointsValue();
            } else {
                $this->userActivityService->createAction($activity, $user);
                $sum += $activity->getPointsValue();
            }
        }

        $pointRepository = $this->entityManager->getRepository(Point::class);
        $points = $pointRepository->sumUserBalance($user);

        $this->assertEquals($points, $sum);
    }

    public function getActivity(): array
    {
        $deliveryBooster = (new Booster())
                ->setName('Dlivery booster')
                ->setPointsValue(5)
                ->setActionCondition(5)
                ->setHourCondition(2);
        $delivery = (new Delivery())
            ->setName('Delivery')
            ->setPointsValue(1)
            ->setBooster($deliveryBooster);
        $deliveryBooster->setActivity($delivery);

        $rideshereBooster = (new Booster())
            ->setName('Rideshare booster')
            ->setPointsValue(5)
            ->setActionCondition(5)
            ->setHourCondition(2);
        $rideshere = (new Rideshare())
            ->setName('Rideshare')
            ->setPointsValue(1)
            ->setBooster($rideshereBooster);
        $rideshereBooster->setActivity($rideshere);

        return [
            [
                [
                    $delivery,
                ],
                [
                    $rideshere,
                ],
                [
                    (new Rent())
                        ->setName('Rent')
                        ->setPointsValue(2),
                ],
            ],
        ];
    }

    protected function tearDown(): void
    {
        $this->entityManager->getConnection()->rollback();

        parent::tearDown();

        $this->entityManager->close();
        $this->entityManager = null;
    }
}
