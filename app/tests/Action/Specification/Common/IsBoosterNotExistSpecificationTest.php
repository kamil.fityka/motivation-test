<?php

declare(strict_types=1);

namespace App\Tests\Action\Service\Specification;

use App\Action\Specification\Common\IsBoosterNotExistSpecification;
use App\Entity\Action\Booster;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class IsBoosterNotExistSpecificationTest extends KernelTestCase
{
    public function testBoosterExist(): void
    {
        $booster = (new Booster())
            ->setName('Dlivery booster')
            ->setPointsValue(5)
            ->setActionCondition(5)
            ->setHourCondition(2);

        $specification = new IsBoosterNotExistSpecification();
        $result = $specification->isSatisfiedBy($booster);

        $this->assertTrue($result);
    }

    public function testBoosterNotExist(): void
    {
        $booster = null;

        $specification = new IsBoosterNotExistSpecification();
        $result = $specification->isSatisfiedBy($booster);

        $this->assertFalse($result);
    }
}
