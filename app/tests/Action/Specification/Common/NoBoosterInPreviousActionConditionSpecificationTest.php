<?php

declare(strict_types=1);

namespace App\Tests\Action\Service\Specification;

use App\Action\Specification\Common\NoBoosterInPreviousActionConditionSpecification;
use App\Entity\Action\Booster;
use App\Entity\Point\Point;
use App\Point\Repository\PointRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class NoBoosterInPreviousActionConditionSpecificationTest extends KernelTestCase
{
    private ?EntityManagerInterface $entityManager;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->entityManager->getConnection()->beginTransaction();
    }

    public function testActitiviesWithoutBooster()
    {
        $booster = (new Booster())
            ->setName('Dlivery booster')
            ->setPointsValue(5)
            ->setActionCondition(5)
            ->setHourCondition(2);

        $repository = $this->createMock(PointRepository::class);
        $repository->method('findLastActionsByBoosterActionConditions')
            ->willReturn([
                (new Point())->setBooster(null),
                (new Point())->setBooster(null),
                (new Point())->setBooster(null),
                (new Point())->setBooster(null),
                (new Point())->setBooster(null),
                (new Point())->setBooster(null),
            ]);

        $specification = new NoBoosterInPreviousActionConditionSpecification($repository);
        $result = $specification->isSatisfiedBy($booster);

        $this->assertTrue($result);
    }

    public function testActitiviesWithBooster()
    {
        $booster = (new Booster())
            ->setName('Dlivery booster')
            ->setPointsValue(5)
            ->setActionCondition(5)
            ->setHourCondition(2);

        $repository = $this->createMock(PointRepository::class);
        $repository->method('findLastActionsByBoosterActionConditions')
            ->willReturn([
                (new Point())->setBooster(null),
                (new Point())->setBooster($booster),
                (new Point())->setBooster(null),
            ]);

        $specification = new NoBoosterInPreviousActionConditionSpecification($repository);
        $result = $specification->isSatisfiedBy($booster);

        $this->assertFalse($result);
    }
}
