<?php

declare(strict_types=1);

namespace App\Tests\Action\Service\Specification;

use App\Action\Specification\Common\MoreActivitiesThanBoosterNeedSpecification;
use App\Entity\Action\Booster;
use App\Point\Repository\PointRepository;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class MoreActivitiesThanBoosterNeedSpecificationTest extends KernelTestCase
{
    public function testLessActivities(): void
    {
        $booster = (new Booster())
            ->setName('Dlivery booster')
            ->setPointsValue(5)
            ->setActionCondition(5)
            ->setHourCondition(2);

        $repository = $this->createMock(PointRepository::class);
        $repository->method('countUserActivityBooster')
            ->willReturn(3);

        $specification = new MoreActivitiesThanBoosterNeedSpecification($repository);
        $result = $specification->isSatisfiedBy($booster);

        $this->assertFalse($result);
    }

    public function testMoreActivities(): void
    {
        $booster = (new Booster())
            ->setName('Dlivery booster')
            ->setPointsValue(5)
            ->setActionCondition(5)
            ->setHourCondition(2);

        $repository = $this->createMock(PointRepository::class);
        $repository->method('countUserActivityBooster')
            ->willReturn(7);

        $specification = new MoreActivitiesThanBoosterNeedSpecification($repository);
        $result = $specification->isSatisfiedBy($booster);

        $this->assertTrue($result);
    }
}
