<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220715164650 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE point DROP INDEX UNIQ_B7A5F324F85E4930, ADD INDEX IDX_B7A5F324F85E4930 (booster_id)');
        $this->addSql('ALTER TABLE point DROP INDEX UNIQ_B7A5F3249D32F035, ADD INDEX IDX_B7A5F3249D32F035 (action_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE point DROP INDEX IDX_B7A5F3249D32F035, ADD UNIQUE INDEX UNIQ_B7A5F3249D32F035 (action_id)');
        $this->addSql('ALTER TABLE point DROP INDEX IDX_B7A5F324F85E4930, ADD UNIQUE INDEX UNIQ_B7A5F324F85E4930 (booster_id)');
    }
}
