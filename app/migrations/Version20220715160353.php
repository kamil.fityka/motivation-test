<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220715160353 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE activities (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, points_value INT NOT NULL, type VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_B5F1AFE55E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE booster (id INT AUTO_INCREMENT NOT NULL, activity_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, points_value INT NOT NULL, action_condition INT NOT NULL, hour_condition INT NOT NULL, UNIQUE INDEX UNIQ_EF769FAD81C06096 (activity_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE point (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, action_id INT DEFAULT NULL, booster_id INT DEFAULT NULL, value INT NOT NULL, valid_to DATETIME DEFAULT NULL, paid_out TINYINT(1) DEFAULT 0 NOT NULL, INDEX IDX_B7A5F324A76ED395 (user_id), UNIQUE INDEX UNIQ_B7A5F3249D32F035 (action_id), UNIQUE INDEX UNIQ_B7A5F324F85E4930 (booster_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE booster ADD CONSTRAINT FK_EF769FAD81C06096 FOREIGN KEY (activity_id) REFERENCES activities (id)');
        $this->addSql('ALTER TABLE point ADD CONSTRAINT FK_B7A5F324A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE point ADD CONSTRAINT FK_B7A5F3249D32F035 FOREIGN KEY (action_id) REFERENCES activities (id)');
        $this->addSql('ALTER TABLE point ADD CONSTRAINT FK_B7A5F324F85E4930 FOREIGN KEY (booster_id) REFERENCES booster (id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE booster DROP FOREIGN KEY FK_EF769FAD81C06096');
        $this->addSql('ALTER TABLE point DROP FOREIGN KEY FK_B7A5F3249D32F035');
        $this->addSql('ALTER TABLE point DROP FOREIGN KEY FK_B7A5F324F85E4930');
        $this->addSql('ALTER TABLE point DROP FOREIGN KEY FK_B7A5F324A76ED395');
        $this->addSql('DROP TABLE activities');
        $this->addSql('DROP TABLE booster');
        $this->addSql('DROP TABLE point');
        $this->addSql('DROP TABLE user');
    }
}
